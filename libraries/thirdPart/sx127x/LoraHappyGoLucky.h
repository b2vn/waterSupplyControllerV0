/*
 * LoraHappyGoLucky.h
 *
 *  Created on: 24/07/2015
 *      Author: nikolaj
 */

#ifndef THIRDPART_SX127X_LORAHAPPYGOLUCKY_H_
#define THIRDPART_SX127X_LORAHAPPYGOLUCKY_H_

#include <src/settings.h>
#if USE_LORA


#include "iSX127x.h"

//LORA BANDWIDTH:
const uint8_t BW_125 = 0x00;
const uint8_t BW_250 = 0x01;
const uint8_t BW_500 = 0x02;
const double SignalBwLog[] = { 5.0969100130080564143587833158265,
		5.397940008672037609572522210551, 5.6989700043360188047862611052755 };

//LORA CODING RATE:
const uint8_t CR_5 = 0x01;
const uint8_t CR_6 = 0x02;
const uint8_t CR_7 = 0x03;
const uint8_t CR_8 = 0x04;

//LORA SPREADING FACTOR:
const uint8_t SF_6 = 0x06;
const uint8_t SF_7 = 0x07;
const uint8_t SF_8 = 0x08;
const uint8_t SF_9 = 0x09;
const uint8_t SF_10 = 0x0A;
const uint8_t SF_11 = 0x0B;
const uint8_t SF_12 = 0x0C;

//LORA MODES:
const uint8_t LORA_SLEEP_MODE = 0x80;
const uint8_t LORA_STANDBY_MODE = 0x81;
const uint8_t LORA_TX_MODE = 0x83;
const uint8_t LORA_RX_MODE = 0x85;
const uint8_t LORA_STANDBY_FSK_REGS_MODE = 0xC1;

class LoraHappyGoLucky: public ISX127x {
protected:
	uint8_t SX1272_SS; // Power toggle pin
public:
	LoraHappyGoLucky();
	virtual ~LoraHappyGoLucky();

	uint8_t ON();
	void OFF();

	byte readRegister(byte address);

	void writeRegister(byte address, byte data);

	void clearFlags();

	uint8_t getMode();

	int8_t setMode(uint8_t mode);

	uint8_t getHeader();

	int8_t setHeaderON();

	int8_t setHeaderOFF();

	uint8_t getCRC();

	uint8_t setCRC_ON();

	uint8_t setCRC_OFF();

	boolean isSF(uint8_t spr);

	int8_t getSF();

	uint8_t setSF(uint8_t spr);

	boolean isBW(uint16_t band);

	int8_t getBW();

	int8_t setBW(uint16_t band);

	boolean isCR(uint8_t cod);

	int8_t getCR();

	int8_t setCR(uint8_t cod);

	boolean isChannel(uint32_t ch);

	uint8_t getChannel();

	int8_t setChannel(uint32_t ch);

	uint8_t getPower();

	int8_t setPower(char p);

	int8_t setPowerNum(uint8_t pow);

	uint8_t getPreambleLength();

	uint8_t setPreambleLength(uint16_t l);

	uint8_t getPayloadLength();

	int8_t setPacketLength();

	int8_t setPacketLength(uint8_t l);

	uint8_t getNodeAddress();

	int8_t setNodeAddress(uint8_t addr);

	int8_t getSNR();

	uint8_t getRSSI();

	int16_t getRSSIpacket();

	uint8_t setRetries(uint8_t ret);

	uint8_t getMaxCurrent();

	int8_t setMaxCurrent(uint8_t rate);

	uint8_t getRegs();

	uint8_t truncPayload(uint16_t length16);

	uint8_t setACK();

	uint8_t receive();

	uint8_t receivePacketMAXTimeout();

	uint8_t receivePacketTimeout();

	uint8_t receivePacketTimeout(uint16_t wait);

	uint8_t receivePacketMAXTimeoutACK();

	uint8_t receivePacketTimeoutACK();

	uint8_t receivePacketTimeoutACK(uint16_t wait);

	uint8_t receiveAll();

	uint8_t receiveAll(uint16_t wait);

	boolean availableData();

	boolean availableData(uint16_t wait);

	uint8_t setPacket(uint8_t dest, char *payload);

	uint8_t setPacket(uint8_t dest, uint8_t *payload);

	uint8_t getPacketMAXTimeout();

	int8_t getPacket();

	int8_t getPacket(uint16_t wait);

	uint8_t sendWithMAXTimeout();

	uint8_t sendWithTimeout();

	uint8_t sendWithTimeout(uint16_t wait);

	uint8_t sendPacketMAXTimeout(uint8_t dest, char *payload);

	uint8_t sendPacketMAXTimeout(uint8_t dest, uint8_t *payload,
			uint16_t length);

	uint8_t sendPacketTimeout(uint8_t dest, char *payload);

	uint8_t sendPacketTimeout(uint8_t dest, uint8_t *payload, uint16_t length);

	uint8_t sendPacketTimeout(uint8_t dest, char *payload, uint16_t wait);

	uint8_t sendPacketTimeout(uint8_t dest, uint8_t *payload, uint16_t length,
			uint16_t wait);

	uint8_t sendPacketMAXTimeoutACK(uint8_t dest, char *payload);

	uint8_t sendPacketMAXTimeoutACK(uint8_t dest, uint8_t *payload,
			uint16_t length);

	uint8_t sendPacketTimeoutACK(uint8_t dest, char *payload);

	uint8_t sendPacketTimeoutACK(uint8_t dest, uint8_t *payload,
			uint16_t length);

	uint8_t sendPacketTimeoutACK(uint8_t dest, char *payload, uint16_t wait);

	uint8_t sendPacketTimeoutACK(uint8_t dest, uint8_t *payload,
			uint16_t length, uint16_t wait);

	int8_t setDestination(uint8_t dest);

	uint8_t setTimeout();

	uint8_t setPayload(char *payload);

	uint8_t setPayload(uint8_t *payload);

	uint8_t getACK();

	uint8_t getACK(uint16_t wait);

	uint8_t sendPacketMAXTimeoutACKRetries(uint8_t dest, char *payload);

	uint8_t sendPacketMAXTimeoutACKRetries(uint8_t dest, uint8_t *payload,
			uint16_t length);

	uint8_t sendPacketTimeoutACKRetries(uint8_t dest, char *payload);

	uint8_t sendPacketTimeoutACKRetries(uint8_t dest, uint8_t *payload,
			uint16_t length);

	uint8_t sendPacketTimeoutACKRetries(uint8_t dest, char *payload,
			uint16_t wait);

	uint8_t sendPacketTimeoutACKRetries(uint8_t dest, uint8_t *payload,
			uint16_t length, uint16_t wait);

	uint8_t getTemp();

};

#endif /* THIRDPART_SX127X_LORAHAPPYGOLUCKY_H_ */
#endif //USE_LORA
