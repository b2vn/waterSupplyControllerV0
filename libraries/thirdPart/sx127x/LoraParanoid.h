/*
 * LoraParanoid.h
 *
 *  Created on: 24/07/2015
 *      Author: nikolaj
 */

#ifndef THIRDPART_SX127X_LORAPARANOID_H_
#define THIRDPART_SX127X_LORAPARANOID_H_

#include <src/settings.h>
#if USE_LORA


#include <LoraHappyGoLucky.h>

class LoraParanoid : public LoraHappyGoLucky {
public:
	LoraParanoid();
	virtual ~LoraParanoid();
};

#endif /* THIRDPART_SX127X_LORAPARANOID_H_ */
#endif //USE_LORA
