/*
 *  Library for LoRa 868 / 915MHz SX1272 LoRa module
 *
 *  Copyright (C) Libelium Comunicaciones Distribuidas S.L.
 *  http://www.libelium.com
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/.
 *
 *  Version:           1.1
 *  Design:            David Gascón
 *  Implementation:    Covadonga Albiñana & Victor Boria
 */


#ifdef SX1272_h
#define SX1272_h

#include <src/settings.h>
#if USE_LORA


/******************************************************************************
 * Includes
 ******************************************************************************/

#include <stdlib.h>
#include <stdint.h>
#include <Arduino.h>
#include <SPI.h>

#ifndef inttypes_h
	#include <inttypes.h>
#endif

#include "ISMChannels.h"
#include "Registers.h"
#include "sxSettings.h"
#include "pack.h"

/******************************************************************************
 * Definitions & Declarations
 *****************************************************************************/

#define SX1272_debug_mode 0

#define SX1272_SS 2

//! MACROS //
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)  // read a bit
#define bitSet(value, bit) ((value) |= (1UL << (bit)))    // set bit to '1'
#define bitClear(value, bit) ((value) &= ~(1UL << (bit))) // set bit to '0'


//LORA BANDWIDTH:
const uint8_t BW_125 = 0x00;
const uint8_t BW_250 = 0x01;
const uint8_t BW_500 = 0x02;
const double SignalBwLog[] =
{
    5.0969100130080564143587833158265,
    5.397940008672037609572522210551,
    5.6989700043360188047862611052755
};

//LORA CODING RATE:
const uint8_t CR_5 = 0x01;
const uint8_t CR_6 = 0x02;
const uint8_t CR_7 = 0x03;
const uint8_t CR_8 = 0x04;

//LORA SPREADING FACTOR:
const uint8_t SF_6 = 0x06;
const uint8_t SF_7 = 0x07;
const uint8_t SF_8 = 0x08;
const uint8_t SF_9 = 0x09;
const uint8_t SF_10 = 0x0A;
const uint8_t SF_11 = 0x0B;
const uint8_t SF_12 = 0x0C;

//LORA MODES:
const uint8_t LORA_SLEEP_MODE = 0x80;
const uint8_t LORA_STANDBY_MODE = 0x81;
const uint8_t LORA_TX_MODE = 0x83;
const uint8_t LORA_RX_MODE = 0x85;
const uint8_t LORA_STANDBY_FSK_REGS_MODE = 0xC1;

//FSK MODES:
const uint8_t FSK_SLEEP_MODE = 0x00;
const uint8_t FSK_STANDBY_MODE = 0x01;
const uint8_t FSK_TX_MODE = 0x03;
const uint8_t FSK_RX_MODE = 0x05;

//OTHER CONSTANTS:
const uint8_t HEADER_ON = 0;
const uint8_t HEADER_OFF = 1;
const uint8_t CRC_ON = 1;
const uint8_t CRC_OFF = 0;
const uint8_t LORA = 1;
const uint8_t FSK = 0;



//! Structure :
/*!
 */


/******************************************************************************
 * Class
 ******************************************************************************/

//! SX1272 Class
/*!
	SX1272 Class defines all the variables and functions used to manage
	SX1272 modules.
 */
class SX1272
{

public:

	//! class constructor
  	/*!
	It does nothing
	\param void
	\return void
  	 */
   	SX1272();

	//! It puts the module ON
  	/*!
	\param void
	\return uint8_t setLORA state
	 */
	uint8_t ON();

	//! It puts the module OFF
  	/*!
	\param void
	\return void
	 */
	void OFF();

	//! It reads an internal module register.
  	/*!
  	\param byte address : address register to read from.
	\return the content of the register.
	 */
	byte readRegister(byte address);

	//! It writes in an internal module register.
  	/*!
  	\param byte address : address register to write in.
  	\param byte data : value to write in the register.
	 */
	void writeRegister(byte address, byte data);

	//! It clears the interruption flags.
  	/*!
	\param void
	\return void
	 */
	void clearFlags();

	//! It sets the LoRa mode on.
  	/*!
  	It stores in global '_LORA' variable '1' when success
    \return '0' on success, '1' otherwise
	 */
	uint8_t setLORA();

	//! It sets the FSK mode on.
  	/*!
  	It stores in global '_FSK' variable '1' when success
	\return '0' on success, '1' otherwise
	 */
	uint8_t setFSK();

	//! It gets the BW, SF and CR of the module.
  	/*!
	It stores in global '_bandwidth' variable the BW
	It stores in global '_codingRate' variable the CR
	It stores in global '_spreadingFactor' variable the SF
	\return '0' on success, '1' otherwise
	 */
	uint8_t getMode();

	//! It sets the BW, SF and CR of the module.
  	/*!
	It stores in global '_bandwidth' variable the BW
	It stores in global '_codingRate' variable the CR
	It stores in global '_spreadingFactor' variable the SF
	\param uint8_t mode : there is a mode number to different values of
	the	configured parameters with this function.
	\return '0' on success, '1' otherwise
	 */
	int8_t setMode(uint8_t mode);

	//! It gets the header mode configured.
  	/*!
  	It stores in global '_header' variable '0' when header is sent
  	(explicit header mode) or '1' when is not sent (implicit header
  	mode).
	\return '0' on success, '1' otherwise
	 */
	uint8_t	getHeader();

	//! It sets explicit header mode.
  	/*!
  	It stores in global '_header' variable '1' when success
	\return '0' on success, '1' otherwise
	 */
	int8_t	setHeaderON();

	//! It sets implicit header mode.
  	/*!
  	It stores in global '_header' variable '0' when success
	\return '0' on success, '1' otherwise
	 */
	int8_t	setHeaderOFF();

	//! It gets the CRC configured.
  	/*!
  	It stores in global '_CRC' variable '1' enabling CRC generation on
  	payload, or '0' disabling the CRC.
	\return '0' on success, '1' otherwise
	 */
	uint8_t	getCRC();

	//! It sets CRC on.
  	/*!
  	It stores in global '_CRC' variable '1' when success
	\return '0' on success, '1' otherwise
	 */
	uint8_t	setCRC_ON();

	//! It sets CRC off.
  	/*!
  	It stores in global '_CRC' variable '0' when success
	\return '0' on success, '1' otherwise
	 */
	uint8_t	setCRC_OFF();

	//! It is true if the SF selected exists.
  	/*!
	\param uint8_t spr : spreading factor value to check.
	\return 'true' on success, 'false' otherwise
	 */
	boolean	isSF(uint8_t spr);

	//! It gets the SF configured.
  	/*!
	It stores in global '_spreadingFactor' variable the current value of SF
	\return '0' on success, '1' otherwise
	 */
	int8_t	getSF();

	//! It sets the SF.
  	/*!
	It stores in global '_spreadingFactor' variable the current value of SF
	\param uint8_t spr : spreading factor value to set in the configuration.
	\return '0' on success, '1' otherwise
	 */
	uint8_t	setSF(uint8_t spr);

	//! It is true if the BW selected exists.
  	/*!
	\param uint16_t band : bandwidth value to check.
	\return 'true' on success, 'false' otherwise
	 */
	boolean	isBW(uint16_t band);

	//! It gets the BW configured.
  	/*!
	It stores in global '_bandwidth' variable the BW selected
	in the configuration
	\return '0' on success, '1' otherwise
	 */
	int8_t	getBW();

	//! It sets the BW.
  	/*!
	It stores in global '_bandwidth' variable the BW selected
	in the configuration
	\param uint16_t band : bandwidth value to set in the configuration.
	\return '0' on success, '1' otherwise
	 */
	int8_t setBW(uint16_t band);

	//! It is true if the CR selected exists.
  	/*!
	\param uint8_t cod : the coding rate value to check.
	\return 'true' on success, 'false' otherwise
	 */
	boolean	isCR(uint8_t cod);

	//! It gets the CR configured.
  	/*!
	It stores in global '_codingRate' variable the CR selected
	in the configuration
	\return '0' on success, '1' otherwise
	 */
	int8_t	getCR();

	//! It sets the CR.
  	/*!
	It stores in global '_codingRate' variable the CR selected
	in the configuration
	\param uint8_t cod : coding rate value to set in the configuration.
	\return '0' on success, '1' otherwise
	 */
	int8_t	setCR(uint8_t cod);


	//! It is true if the channel selected exists.
  	/*!
	\param uint32_t ch : frequency channel value to check.
	\return 'true' on success, 'false' otherwise
	 */
	boolean isChannel(uint32_t ch);

	//! It gets frequency channel the module is using.
  	/*!
	It stores in global '_channel' variable the frequency channel
	\return '0' on success, '1' otherwise
	 */
	uint8_t getChannel();

	//! It sets frequency channel the module is using.
  	/*!
	It stores in global '_channel' variable the frequency channel
	\param uint32_t ch : frequency channel value to set in the configuration.
	\return '0' on success, '1' otherwise
	 */
	int8_t setChannel(uint32_t ch);

	//! It gets the output power of the signal.
  	/*!
	It stores in global '_power' variable the output power of the signal
	\return '0' on success, '1' otherwise
	 */
	uint8_t getPower();

	//! It sets the output power of the signal.
  	/*!
	It stores in global '_power' variable the output power of the signal
	\param char p : 'M', 'H' or 'L' if you want Maximum, High or Low
	output power signal.
	\return '0' on success, '1' otherwise
	 */
	int8_t setPower(char p);

	//! It sets the output power of the signal.
  	/*!
	It stores in global '_power' variable the output power of the signal
	\param uint8_t pow : value to set as output power.
	\return '0' on success, '1' otherwise
	 */
	int8_t setPowerNum(uint8_t pow);

	//! It gets the preamble length configured.
  	/*!
	It stores in global '_preamblelength' variable the preamble length
	\return '0' on success, '1' otherwise
	 */
	uint8_t getPreambleLength();

	//! It sets the preamble length.
  	/*!
    It stores in global '_preamblelength' variable the preamble length
  	\param uint16_t l : preamble length to set in the configuration.
	\return '0' on success, '1' otherwise
	 */
	uint8_t setPreambleLength(uint16_t l);

	//! It gets the payload length of the last packet to send/receive.
  	/*!
    It stores in global '_payloadlength' variable the payload length of
    the last packet to send/receive.
	\return '0' on success, '1' otherwise
	 */
	uint8_t getPayloadLength();

	//! It sets the packet length to send/receive.
  	/*!
  	It stores in global '_payloadlength' variable the payload length of
    the last packet to send/receive.
	\return '0' on success, '1' otherwise
	 */
	int8_t setPacketLength();

	//! It sets the packet length to send/receive.
  	/*!
  	It stores in global '_payloadlength' variable the payload length of
    the last packet to send/receive.
  	\param uint8_t l : payload length to set in the configuration.
	\return '0' on success, '1' otherwise
	 */
	int8_t setPacketLength(uint8_t l);

	//! It gets the node address of the mote.
  	/*!
  	It stores in global '_nodeAddress' variable the node address
	\return '0' on success, '1' otherwise
	 */
	uint8_t getNodeAddress();

	//! It sets the node address of the mote.
  	/*!
  	It stores in global '_nodeAddress' variable the node address
  	\param uint8_t addr : address value to set as node address.
	\return '0' on success, '1' otherwise
	 */
	int8_t setNodeAddress(uint8_t addr);

	//! It gets the SNR of the latest received packet.
  	/*!
	It stores in global '_SNR' variable the SNR
	\return '0' on success, '1' otherwise
	 */
	int8_t getSNR();

	//! It gets the current value of RSSI.
  	/*!
	It stores in global '_RSSI' variable the current value of RSSI
	\return '0' on success, '1' otherwise
	 */
	uint8_t getRSSI();

	//! It gets the RSSI of the latest received packet.
  	/*!
	It stores in global '_RSSIpacket' variable the RSSI of the latest
	packet received.
	\return '0' on success, '1' otherwise
	 */
	int16_t getRSSIpacket();

	//! It sets the total of retries when a packet is not correctly received.
	/*!
	It stores in global '_maxRetries' variable the number of retries.
  	\param uint8_t ret : number of retries.
	\return '0' on success, '1' otherwise
	 */
	uint8_t setRetries(uint8_t ret);

	//! It gets the maximum current supply by the module.
	/*!
  	 *
	\return '0' on success, '1' otherwise
	 */
	uint8_t getMaxCurrent();

	//! It sets the maximum current supply by the module.
	/*!
	It stores in global '_maxCurrent' variable the maximum current supply.
  	\param uint8_t rate : maximum current supply.
	\return '0' on success, '1' otherwise
	 */
	int8_t setMaxCurrent(uint8_t rate);

	//! It gets the content of the main configuration registers.
  	/*!
	It stores in global '_bandwidth' variable the BW.
	It stores in global '_codingRate' variable the CR.
	It stores in global '_spreadingFactor' variable the SF.
	It stores in global '_power' variable the output power of the signal.
  	It stores in global '_channel' variable the frequency channel.
  	It stores in global '_CRC' variable '1' enabling CRC generation on
  	payload, or '0' disabling the CRC.
  	It stores in global '_header' variable '0' when header is sent
  	(explicit header mode) or '1' when is not sent (implicit header
  	mode).
	It stores in global '_preamblelength' variable the preamble length.
    It stores in global '_payloadlength' variable the payload length of
    the last packet to send/receive.
  	It stores in global '_nodeAddress' variable the node address.
	It stores in global '_temp' variable the module temperature.
	\return '0' on success, '1' otherwise
	 */
	uint8_t getRegs();

	//! It sets the maximum number of bytes from a frame that fit in a packet structure.
	/*!
	It stores in global '_payloadlength' variable the maximum number of bytes.
  	\param uint16_t length16 : total frame length.
	\return '0' on success, '1' otherwise
	 */
	uint8_t truncPayload(uint16_t length16);

	//! It writes an ACK in FIFO to send it.
	/*!
	 *
	\return '0' on success, '1' otherwise
	*/
	uint8_t setACK();

	//! It puts the module in reception mode.
  	/*!
  	 *
	\return '0' on success, '1' otherwise
	 */
	uint8_t receive();

	//! It receives a packet before MAX_TIMEOUT.
  	/*!
  	 *
	\return '0' on success, '1' otherwise
	 */
	uint8_t receivePacketMAXTimeout();

	//! It receives a packet before a timeout.
  	/*!
  	 *
	\return '0' on success, '1' otherwise
	 */
	uint8_t receivePacketTimeout();

	//! It receives a packet before a timeout.
  	/*!
  	\param uint16_t wait : time to wait to receive something.
	\return '0' on success, '1' otherwise
	 */
	uint8_t receivePacketTimeout(uint16_t wait);

	//! It receives a packet before MAX_TIMEOUT and reply with an ACK.
  	/*!
  	 *
	\return '0' on success, '1' otherwise
	 */
	uint8_t receivePacketMAXTimeoutACK();

	//! It receives a packet before a timeout and reply with an ACK.
  	/*!
  	 *
	\return '0' on success, '1' otherwise
	 */
	uint8_t receivePacketTimeoutACK();

	//! It receives a packet before a timeout and reply with an ACK.
  	/*!
  	\param uint16_t wait : time to wait to receive something.
	\return '0' on success, '1' otherwise
	 */
	uint8_t receivePacketTimeoutACK(uint16_t wait);

	//! It puts the module in 'promiscuous' reception mode.
  	/*!
  	 *
	\return '0' on success, '1' otherwise
	 */
	uint8_t receiveAll();

	//! It puts the module in 'promiscuous' reception mode with a timeout.
  	/*!
  	\param uint16_t wait : time to wait to receive something.
	\return '0' on success, '1' otherwise
	 */
	uint8_t receiveAll(uint16_t wait);

	//! It checks if there is an available packet and its destination.
  	/*!
  	 *
	\return 'true' on success, 'false' otherwise
	 */
	boolean	availableData();

	//! It checks if there is an available packet and its destination before a timeout.
  	/*!
  	 *
  	\param uint16_t wait : time to wait while there is no a valid header received.
	\return 'true' on success, 'false' otherwise
	 */
	boolean	availableData(uint16_t wait);

	//! It writes a packet in FIFO in order to send it.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\return '0' on success, '1' otherwise
	*/
	uint8_t setPacket(uint8_t dest, char *payload);

	//! It writes a packet in FIFO in order to send it.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t *payload: packet payload.
	\return '0' on success, '1' otherwise
	*/
	uint8_t setPacket(uint8_t dest, uint8_t *payload);

	//! It reads a received packet from the FIFO, if it arrives before ending MAX_TIMEOUT time.
	/*!
	 *
	\return '0' on success, '1' otherwise
	*/
	uint8_t getPacketMAXTimeout();

	//! It reads a received packet from the FIFO, if it arrives before ending '_sendTime' time.
	/*!
	 *
	\return '0' on success, '1' otherwise
	*/
	int8_t getPacket();

	//! It receives and gets a packet from FIFO, if it arrives before ending 'wait' time.
	/*!
	 *
	\param uint16_t wait : time to wait while there is no a complete packet received.
	\return '0' on success, '1' otherwise
	*/
	int8_t getPacket(uint16_t wait);

	//! It sends the packet stored in FIFO before ending MAX_TIMEOUT.
	/*!
	 *
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendWithMAXTimeout();

	//! It sends the packet stored in FIFO before ending _sendTime time.
	/*!
	 *
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendWithTimeout();

	//! It tries to send the packet stored in FIFO before ending 'wait' time.
	/*!
	\param uint16_t wait : time to wait to send the packet.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendWithTimeout(uint16_t wait);

	//! It tries to send the packet wich payload is a parameter before ending MAX_TIMEOUT.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketMAXTimeout(uint8_t dest, char *payload);

	//! It tries to send the packet wich payload is a parameter before ending MAX_TIMEOUT.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t *payload : packet payload.
	\param uint16_t length : payload buffer length.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketMAXTimeout(uint8_t dest, uint8_t *payload, uint16_t length);


	//! It sends the packet wich payload is a parameter before ending MAX_TIMEOUT.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeout(uint8_t dest, char *payload);

	//! It sends the packet wich payload is a parameter before ending MAX_TIMEOUT.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t *payload: packet payload.
	\param uint16_t length : payload buffer length.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeout(uint8_t dest, uint8_t *payload, uint16_t length);

	//! It sends the packet wich payload is a parameter before ending 'wait' time.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\param uint16_t wait : time to wait.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeout(uint8_t dest, char *payload, uint16_t wait);

	//! It sends the packet wich payload is a parameter before ending 'wait' time.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t *payload : packet payload.
	\param uint16_t length : payload buffer length.
	\param uint16_t wait : time to wait.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeout(uint8_t dest, uint8_t *payload, uint16_t length, uint16_t wait);

	//! It sends the packet wich payload is a parameter before MAX_TIMEOUT, and replies with ACK.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketMAXTimeoutACK(uint8_t dest, char *payload);

	//! It sends the packet wich payload is a parameter before MAX_TIMEOUT, and replies with ACK.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t payload: packet payload.
	\param uint16_t length : payload buffer length.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketMAXTimeoutACK(uint8_t dest, uint8_t *payload, uint16_t length);

	//! It sends the packet wich payload is a parameter before a timeout, and replies with ACK.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeoutACK(uint8_t dest, char *payload);

	//! It sends the packet wich payload is a parameter before a timeout, and replies with ACK.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t payload: packet payload.
	\param uint16_t length : payload buffer length.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeoutACK(uint8_t dest, uint8_t *payload, uint16_t length);

	//! It sends the packet wich payload is a parameter before 'wait' time, and replies with ACK.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\param uint16_t wait : time to wait to send the packet.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeoutACK(uint8_t dest, char *payload, uint16_t wait);

	//! It sends the packet wich payload is a parameter before 'wait' time, and replies with ACK.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t payload: packet payload.
	\param uint16_t length : payload buffer length.
	\param uint16_t wait : time to wait to send the packet.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeoutACK(uint8_t dest, uint8_t *payload, uint16_t length, uint16_t wait);

	//! It sets the destination of a packet.
  	/*!
  	\param uint8_t dest : value to set as destination address.
	\return '0' on success, '1' otherwise
	 */
	int8_t setDestination(uint8_t dest);

	//! It sets the waiting time to send a packet.
  	/*!
   	It stores in global '_sendTime' variable the time for each mode.
	\return '0' on success, '1' otherwise
	 */
	uint8_t setTimeout();

	//! It sets the payload of the packet that is going to be sent.
  	/*!
  	\param char *payload : packet payload.
	\return '0' on success, '1' otherwise
	 */
	uint8_t setPayload(char *payload);

	//! It sets the payload of the packet that is going to be sent.
  	/*!
  	\param uint8_t payload: packet payload.
	\return '0' on success, '1' otherwise
	 */
	uint8_t setPayload(uint8_t *payload);

	//! If an ACK is received, it gets it and checks its content.
	/*!
	 *
	\return '0' on success, '1' otherwise
	*/
	uint8_t getACK();

 	//! It receives and gets an ACK from FIFO, if it arrives before ending 'wait' time.
	/*!
	 *
	\param uint16_t wait : time to wait while there is no an ACK received.
	\return '0' on success, '1' otherwise
	*/
	uint8_t getACK(uint16_t wait);

	//! It sends a packet, waits to receive an ACK and updates the _retries value, before ending MAX_TIMEOUT time.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketMAXTimeoutACKRetries(uint8_t dest, char *payload);

	//! It sends a packet, waits to receive an ACK and updates the _retries value, before ending MAX_TIMEOUT time.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t *payload : packet payload.
	\param uint16_t length : payload buffer length.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketMAXTimeoutACKRetries(uint8_t dest, uint8_t *payload, uint16_t length);

	//! It sends a packet, waits to receive an ACK and updates the _retries value.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeoutACKRetries(uint8_t dest, char *payload);

	//! It sends a packet, waits to receive an ACK and updates the _retries value.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t *payload : packet payload.
	\param uint16_t length : payload buffer length.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeoutACKRetries(uint8_t dest, uint8_t *payload, uint16_t length);

	//! It sends a packet, waits to receive an ACK and updates the _retries value, before ending 'wait' time.
	/*!
	\param uint8_t dest : packet destination.
	\param char *payload : packet payload.
	\param uint16_t wait : time to wait while trying to send the packet.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeoutACKRetries(uint8_t dest, char *payload, uint16_t wait);

	//! It sends a packet, waits to receive an ACK and updates the _retries value, before ending 'wait' time.
	/*!
	\param uint8_t dest : packet destination.
	\param uint8_t *payload : packet payload.
	\param uint16_t length : payload buffer length.
	\param uint16_t wait : time to wait while trying to send the packet.
	\return '0' on success, '1' otherwise
	*/
	uint8_t sendPacketTimeoutACKRetries(uint8_t dest, uint8_t *payload, uint16_t length, uint16_t wait);

	//! It gets the internal temperature of the module.
	/*!
	It stores in global '_temp' variable the module temperature.
	\return '0' on success, '1' otherwise
	*/
	uint8_t getTemp();


	/// Variables /////////////////////////////////////////////////////////////

	//! Variable : bandwidth configured in LoRa mode.
	//!    bandwidth = 00  --> BW = 125KHz
	//!    bandwidth = 01  --> BW = 250KHz
	//!    bandwidth = 10  --> BW = 500KHz
  	/*!
   	*/
	uint8_t _bandwidth;

	//! Variable : coding rate configured in LoRa mode.
	//!    codingRate = 001  --> CR = 4/5
	//!    codingRate = 010  --> CR = 4/6
	//!    codingRate = 011  --> CR = 4/7
	//!    codingRate = 100  --> CR = 4/8
  	/*!
   	*/
	uint8_t _codingRate;

	//! Variable : spreading factor configured in LoRa mode.
	//!    spreadingFactor = 6   --> SF = 6, 64 chips/symbol
	//!    spreadingFactor = 7   --> SF = 7, 128 chips/symbol
	//!    spreadingFactor = 8   --> SF = 8, 256 chips/symbol
	//!    spreadingFactor = 9   --> SF = 9, 512 chips/symbol
	//!    spreadingFactor = 10  --> SF = 10, 1024 chips/symbol
	//!    spreadingFactor = 11  --> SF = 11, 2048 chips/symbol
	//!    spreadingFactor = 12  --> SF = 12, 4096 chips/symbol
  	/*!
   	*/
	uint8_t _spreadingFactor;

	//! Variable : frequency channel.
	//!    channel = 0xD84CCC  --> CH = 10_868, 865.20MHz
	//!    channel = 0xD86000  --> CH = 11_868, 865.50MHz
	//!    channel = 0xD87333  --> CH = 12_868, 865.80MHz
	//!    channel = 0xD88666  --> CH = 13_868, 866.10MHz
	//!    channel = 0xD89999  --> CH = 14_868, 866.40MHz
	//!    channel = 0xD8ACCC  --> CH = 15_868, 866.70MHz
	//!    channel = 0xD8C000  --> CH = 16_868, 867.00MHz
	//!    channel = 0xE1C51E  --> CH = 00_900, 903.08MHz
	//!    channel = 0xE24F5C  --> CH = 01_900, 905.24MHz
	//!    channel = 0xE2D999  --> CH = 02_900, 907.40MHz
	//!    channel = 0xE363D7  --> CH = 03_900, 909.56MHz
	//!    channel = 0xE3EE14  --> CH = 04_900, 911.72MHz
	//!    channel = 0xE47851  --> CH = 05_900, 913.88MHz
	//!    channel = 0xE5028F  --> CH = 06_900, 916.04MHz
	//!    channel = 0xE58CCC  --> CH = 07_900, 918.20MHz
	//!    channel = 0xE6170A  --> CH = 08_900, 920.36MHz
	//!    channel = 0xE6A147  --> CH = 09_900, 922.52MHz
	//!    channel = 0xE72B85  --> CH = 10_900, 924.68MHz
	//!    channel = 0xE7B5C2  --> CH = 11_900, 926.84MHz
  	/*!
   	*/
	uint32_t _channel;

	//! Variable : output power.
	//!
  	/*!
   	*/
	uint8_t _power;

	//! Variable : SNR from the last packet received in LoRa mode.
	//!
  	/*!
   	*/
	int8_t _SNR;

	//! Variable : RSSI current value.
	//!
  	/*!
   	*/
	int8_t _RSSI;

	//! Variable : RSSI from the last packet received in LoRa mode.
	//!
  	/*!
   	*/
	int16_t _RSSIpacket;

	//! Variable : preamble length sent/received.
	//!
  	/*!
   	*/
	uint16_t _preamblelength;

	//! Variable : payload length sent/received.
	//!
  	/*!
   	*/
	uint16_t _payloadlength;

	//! Variable : node address.
	//!
  	/*!
   	*/
	uint8_t _nodeAddress;

	//! Variable : implicit or explicit header in LoRa mode.
	//!
  	/*!
   	*/
	uint8_t _header;

	//! Variable : header received while waiting a packet to arrive.
	//!
  	/*!
   	*/
	uint8_t _hreceived;

	//! Variable : presence or absence of CRC calculation.
	//!
  	/*!
   	*/
	uint8_t _CRC;

	//! Variable : packet destination.
	//!
  	/*!
   	*/
	uint8_t _destination;

	//! Variable : packet number.
	//!
  	/*!
   	*/
	uint8_t _packetNumber;

	//! Variable : indicates if received packet is correct or incorrect.
	//!
  	/*!
   	*/
   	uint8_t _reception;

	//! Variable : number of current retry.
	//!
  	/*!
   	*/
   	uint8_t _retries;

   	//! Variable : maximum number of retries.
	//!
  	/*!
   	*/
   	uint8_t _maxRetries;

   	//! Variable : maximum current supply.
	//!
  	/*!
   	*/
   	uint8_t _maxCurrent;

	//! Variable : indicates FSK or LoRa 'modem'.
	//!
  	/*!
   	*/
	uint8_t _modem;

	//! Variable : array with all the information about a sent packet.
	//!
  	/*!
   	*/
	pack packet_sent;

	//! Variable : array with all the information about a received packet.
	//!
  	/*!
   	*/
	pack packet_received;

	//! Variable : array with all the information about a sent/received ack.
	//!
  	/*!
   	*/
	pack ACK;

	//! Variable : temperature module.
	//!
  	/*!
   	*/
	int _temp;

	//! Variable : current timeout to send a packet.
	//!
  	/*!
   	*/
	uint16_t _sendTime;

};

extern SX1272	sx1272;

#endif
#endif //USE_LORA
