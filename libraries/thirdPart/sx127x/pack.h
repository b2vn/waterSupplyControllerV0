/*
 * pack.h
 *
 *  Created on: 24/07/2015
 *      Author: nikolaj
 */

#ifndef SX127X_PACK_H_
#define SX127X_PACK_H_

#include <src/settings.h>
#if USE_LORA


#include "sxSettings.h"

struct pack
{
	//! Structure Variable : Packet destination
	/*!
 	*/
	uint8_t dst;

	//! Structure Variable : Packet source
	/*!
 	*/
	uint8_t src;

	//! Structure Variable : Packet number
	/*!
 	*/
	uint8_t packnum;

	//! Structure Variable : Packet length
	/*!
 	*/
	uint8_t length;

	//! Structure Variable : Packet payload
	/*!
 	*/
	uint8_t data[MAX_PAYLOAD];

	//! Structure Variable : Retry number
	/*!
 	*/
	uint8_t retry;
};



#endif /* SX127X_PACK_H_ */
#endif //USE_LORA
