/*
 * sxSettings.h
 *
 *  Created on: 24/07/2015
 *      Author: nikolaj
 */

#ifndef SXSETTINGS_H_
#define SXSETTINGS_H_

#include <src/settings.h>
#if USE_LORA


const uint8_t BROADCAST_0 = 0x00;
const uint8_t MAX_LENGTH = 255;

#if !defined(MAX_PAYLOAD)
#define MAX_PAYLOAD 251
#endif

const uint8_t MAX_LENGTH_FSK = 64;
const uint8_t MAX_PAYLOAD_FSK = 60;
const uint8_t ACK_LENGTH = 5;
const uint8_t OFFSET_PAYLOADLENGTH = 5;
const uint8_t OFFSET_RSSI = 137;
const uint8_t NOISE_FIGURE = 6.0;
const uint8_t NOISE_ABSOLUTE_ZERO = 174.0;
const uint16_t MAX_TIMEOUT = 8000;		//8000 msec = 8.0 sec
const uint16_t MAX_WAIT = 12000;		//12000 msec = 12.0 sec
const uint8_t MAX_RETRIES = 5;
const uint8_t CORRECT_PACKET = 0;
const uint8_t INCORRECT_PACKET = 1;




#endif /* SXSETTINGS_H_ */

#endif //USE_LORA
