/*
 * AlongRoadMap.h
 *
 *  Created on: 29/06/2015
 *      Author: nikolaj
 */

#ifndef ALONGROADMAP_H_
#define ALONGROADMAP_H_

#include "Map.h"
#include "Channels.h"
#include "settings.h"

class AlongRoad: public Map {
public:
	void set(Channels *c) {
	    c->setIntended(0, L2P(TREE+5));
	    c->setIntended(1, L2P(MEDIUM_BUSH));
	    c->setIntended(2, L2P(BIG_BUSH));
	    c->setIntended(3, L2P(BIG_BUSH));
	    c->setIntended(4, L2P(MEDIUM_BUSH));
	    c->setIntended(5, L2P(SMALL_TREE));
	    c->setIntended(6, L2P(MEDIUM_BUSH));
	    c->setIntended(7, L2P(BIG_BUSH));
	    c->setIntended(8, L2P(SMALL_TREE));
	    c->setIntended(9, L2P(SMALL_TREE));
	    c->setIntended(10, L2P(SMALL_TREE));
	    c->setIntended(11, L2P(SMALL_BUSH));
	    c->setIntended(12, L2P(BIG_BUSH));
	    c->setIntended(13, L2P(BIG_BUSH));
	    c->setIntended(14, L2P(BIG_BUSH));
	    c->setIntended(15, L2P(SMALL_TREE));
	    c->setIntended(16, L2P(MEDIUM_BUSH));
	    c->setIntended(17, L2P(BIG_BUSH));
	    c->setIntended(18, L2P(BIG_BUSH));
	    c->setIntended(19, L2P(MEDIUM_BUSH));
	    c->setIntended(20, L2P(BIG_BUSH));
	    c->setIntended(21, L2P(SMALL_TREE));
	    c->setIntended(22, L2P(BIG_BUSH));
	    c->setIntended(23, L2P(TREE));
	    c->setIntended(24, L2P(SMALL_BUSH));
	    c->setIntended(25, L2P(SMALL_BUSH));
	    c->setIntended(26, L2P(SMALL_BUSH));
	    c->setIntended(27, L2P(SMALL_TREE));
	    c->setIntended(28, L2P(MEDIUM_BUSH));
	    c->setIntended(29, L2P(SMALL_TREE));
	    c->setIntended(30, L2P(SMALL_TREE));
	    c->setIntended(31, L2P(0));
	    c->setIntended(32, L2P(SMALL_TREE));
	    c->setIntended(33, L2P(TREE));
	    c->setIntended(34, L2P(TREE));
	    c->setIntended(35, L2P(BIG_BUSH));
	    c->setIntended(36, L2P(BIG_BUSH));
	    c->setIntended(37, L2P(SMALL_TREE));
	    c->setIntended(38, L2P(SMALL_BUSH));
	    c->setIntended(39, L2P(SMALL_BUSH));
	    c->setIntended(40, L2P(SMALL_BUSH));
	    c->setIntended(41, L2P(SMALL_BUSH));
	    c->setIntended(42, L2P(SMALL_BUSH));
	    c->setIntended(43, L2P(SMALL_BUSH));
	    c->setIntended(44, L2P(SMALL_BUSH));
	    c->setIntended(45, L2P(SMALL_BUSH));
	    c->setIntended(46, L2P(SMALL_BUSH));
	    c->setIntended(47, L2P(SMALL_TREE));
	}
};

#endif /* ALONGROADMAP_H_ */
