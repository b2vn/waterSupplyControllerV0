/*
 * Channel.cpp
 *
 *  Created on: 29/06/2015
 *      Author: nikolaj
 */

#include "Channels.h"
#include <stddef.h>

Channels::Channels(uint8_t size, Map *m) : current(0) {
	for (uint8_t i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		intended[i] = 0;
		state[i] = cs_NOT_CONFIGURED;
	}
	//      intended[0] = L2P(1);
	//      intended[1] = L2P(1);
	// Set some data just for testing

	if (m != NULL)
		m->set(this);
}

bool Channels::anyOn() {
	for (uint8_t i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		if (state[i] == cs_OPEN)
			return true;
	}
	return false;
}

void Channels::resetAll() {
	// Open the first valvestart
	for (uint8_t i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		if (intended[i] > 0)
			state[i] = cs_CLOSED;
		amount[i] = 0;
	}

	current = 0;

	printStates();
}

void Channels::setIntended(uint8_t channel, uint16_t amount) {
	intended[channel] = amount;
	state[channel] = amount > 0 ? cs_CLOSED : cs_NOT_CONFIGURED;
}

void Channels::printStates() {
	bool anyOn = false;
	// Turn off output power
	digitalWrite(OUTPUT_ENABLE, LOW);
	for (uint8_t i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		if (state[i] == cs_OPEN) {
			Serial.print("1");
			// Set output pin
			shifty.shift(true);
			anyOn = true;
		} else {
			Serial.print("0");
			// Clear output pin
			shifty.shift(false);
		}
	}
	shifty.latch();
	Serial.println();
	// Enable valve output if any of the valves are open.
	if (anyOn)
		digitalWrite(OUTPUT_ENABLE, HIGH);
}

bool Channels::openNext() {
	// TODO: There need to be a single step functionality, so the system pauses when one
	// channel is done. This way it will be possible to verify that the configuration has
	// been made correctly
	bool ret = true;
	uint8_t i;
	for (i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		if (intended[i] > 0 && state[i] != cs_DONE) {
			state[i] = cs_OPEN;
			ret = false;
			break;
		}
	}

	printStates();

	return ret;
}

bool Channels::increment() {
	// TODO: This only supports one channel being active at the time
	for (uint8_t i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		if (state[i] == cs_OPEN) {
			current = i;
			if (amount[i] == 0) {
				Serial.print("Feeding ");
				Serial.println(i);
			}

//			if ((amount[i] % 100) == 0)
//				display->printCurrent(i, 100UL * amount[i] / intended[i]);

			++amount[i];

#if DEBUG_FLOW
			Serial.print("  ");
			Serial.print(amount[i]);
			Serial.print(" of ");
			Serial.println(intended[i]);
#endif

			// Check if done
			if (amount[i] >= intended[i]) {
				state[i] = cs_DONE;
				Serial.println("  done");
				bool ret = openNext();

//				if (ret)
//					// all valves are closed
//					display->printCurrent(-1, -1);

				return ret;
			}

			return false;
		}
	}
	return true;
}

uint16_t Channels::getIntended() {
	uint16_t ret = 0;
	for (uint8_t i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		ret += intended[i];
	}
	return ret;
}

uint16_t Channels::getAmount() {
	uint16_t ret = 0;
	for (uint8_t i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		ret += amount[i];
	}
	return ret;
}

void Channels::stop() {
	// Open the first valvestart
	for (uint8_t i = 0; i < NUMBER_OF_CHANNELS; ++i) {
		if (intended[i] > 0)
			state[i] = cs_DONE;
		amount[i] = 0;
	}
	printStates();
}

uint8_t Channels::getAmount(uint8_t id) {
	return amount[id];
}

uint8_t Channels::getProcentage() {
	uint8_t i = getCurrent();
	uint32_t p = 100ul*amount[i];
	p /= intended[i];

	return (uint8_t)p;
}

uint8_t Channels::getCurrent() {

	return current;
}
