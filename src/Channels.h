/*
 * Channel.h
 *
 *  Created on: 29/06/2015
 *      Author: nikolaj
 */

#ifndef CHANNELS_H_
#define CHANNELS_H_

#include <Arduino.h>
#include "settings.h"
#include "Shifty.h"
#include "Map.h"

	enum {
		cs_NOT_CONFIGURED, cs_CLOSED, cs_OPEN, cs_DONE
	};

class Channels {
protected:

	uint16_t intended[NUMBER_OF_CHANNELS];
	uint16_t amount[NUMBER_OF_CHANNELS];
	uint8_t state[NUMBER_OF_CHANNELS];
	uint8_t current;

	Shifty shifty;

	void printStates();
	uint16_t getIntended();
	uint16_t getAmount();

public:
	Channels(uint8_t size, Map *m);
	bool anyOn();
	void resetAll();
	bool openNext();
	bool increment();
	void stop();
	void setIntended(uint8_t channel, uint16_t amount);

	uint8_t getAmount(uint8_t id);
	uint8_t getProcentage();
	uint8_t getCurrent();
};
#endif /* CHANNELS_H_ */
