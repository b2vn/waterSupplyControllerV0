/*
 * Display.cpp
 *
 *  Created on: 30/06/2015
 *      Author: nikolaj
 */

#include "Display.h"
#include <stdio.h>

// Connections:
// A4 -> SDA
// A5 -> SCL
Display::Display() :
		lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE) {
}

Display::~Display() {
	// TODO Auto-generated destructor stub
}

void Display::begin() {
	lcd.begin(20, 4); // initialize the lcd for 20 chars 4 lines
	// lcd.backlight(); // turn on backlight
	lcd.noBacklight();
}

void Display::booting() {
	lcd.setCursor(4, 0);
	lcd.print("miniSvend");

	lcd.setCursor(0, 1);
	lcd.print("================");

	lcd.setCursor(5, 2);
	lcd.print(__DATE__);

	lcd.setCursor(8, 3);
	lcd.print(__TIME__);
}

void Display::booted() {
	lcd.setCursor(0, 3);
	lcd.print("Ready      ");
}

void Display::init() {
	lcd.on();
	lcd.clear();
	initCurrent(0);
	initFlow(1);
	initPing(2);
	initTotal(3);
}

void Display::off() {
	lcd.off();
}

void Display::printFlow(uint32_t mlps, uint8_t lowCount) {
	// TODO: The line number is not as flexible as in the init function;
	lcd.setCursor(6, 1);
	if (lowCount > 0) {
		lcd.print("(");
		lcd.print(lowCount);
		lcd.print(")");
	} else {
		lcd.print("    ");
	}
	char buffer[6];
	sprintf(buffer, "%5d", mlps);
	lcd.setCursor(10, 1);
	lcd.print(buffer);
}

void Display::printPing(uint32_t ms) {
	// TODO: The line number is not as flexible as in the init function;
	if (ms == (uint32_t) ((-1))) {
		// time out
		lcd.setCursor(12, 2);
		lcd.print("---");
	} else {
		char buffer[6];
		sprintf(buffer, "%5d", ms);
		lcd.setCursor(10, 2);
		lcd.print(buffer);
	}
}

void Display::printTotal(uint32_t l) {
	char buffer[6];
	sprintf(buffer, "%5d", l);
	lcd.setCursor(10, 3);
	lcd.print(buffer);
}

void Display::printCurrent(uint8_t id, uint8_t p) {
	if (id == (uint8_t) -1) {
		lcd.setCursor(12, 0);
		lcd.print(" --");
		lcd.setCursor(15, 0);
		lcd.print("   ");
	} else {
		char buffer[5];
		sprintf(buffer, "%3d", id);
		lcd.setCursor(12, 0);
		lcd.print(buffer);

		sprintf(buffer, "%3d", p);
		lcd.setCursor(15, 0);
		lcd.print(buffer);
	}
}

void Display::printFaucet(bool on) {
	lcd.setCursor(6, 2);
	lcd.print(on ? '+' : '-');
}
