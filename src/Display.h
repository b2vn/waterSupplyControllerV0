/*
 * Display.h
 *
 *  Created on: 30/06/2015
 *      Author: nikolaj
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <LiquidCrystal_I2C.h>

class Display {
protected:
	LiquidCrystal_I2C lcd;

	void initCurrent(uint8_t line) {
		lcd.setCursor(0, line);
		lcd.print("Current:"); 	// "Current:     23  69%"
		lcd.setCursor(19, line);
		lcd.print("%");
	}

	void initFlow(uint8_t line) {
		lcd.setCursor(0, line);
		lcd.print("Flow:"); 	// "Flow: (!)   234 ml/s"
		lcd.setCursor(16, line);
		lcd.print("ml/s");
	}

	void initPing(uint8_t line) {
		lcd.setCursor(0, line);
		lcd.print("Ping:"); 	// "Ping:       123   ms"
		lcd.setCursor(18, line);
		lcd.print("ms");
	}

	void initTotal(uint8_t line) {
		lcd.setCursor(0, line);
		lcd.print("Total:"); 	// "Amount:   21345    l"
		lcd.setCursor(19, line);
		lcd.print("l");
	}

public:
	Display();
	virtual ~Display();

	void begin();
	void booting();
	void booted();

	void init();
	void off();

	void printFlow(uint32_t mlps, uint8_t lowCount);
	void printPing(uint32_t ms);
	void printTotal(uint32_t l);
	void printCurrent(uint8_t id, uint8_t p);
	void printFaucet(bool on);
};
#endif /* DISPLAY_H_ */
