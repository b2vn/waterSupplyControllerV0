/*
 * Map.h
 *
 *  Created on: 29/06/2015
 *      Author: nikolaj
 */

#ifndef MAP_H_
#define MAP_H_

class Channels;

class Map {
public:
	virtual void set(Channels *c) = 0;
	virtual ~Map() {}
};

#endif /* MAP_H_ */
