/*
 * OneLiter32.h
 *
 *  Created on: 01/07/2015
 *      Author: nikolaj
 */

#ifndef ONELITER32_H_
#define ONELITER32_H_

#include "Map.h"

class OneLiter32 : public Map {
public:
	void set(Channels *c){
		c->setIntended(31, L2P(1));
	}
};



#endif /* ONELITER32_H_ */
