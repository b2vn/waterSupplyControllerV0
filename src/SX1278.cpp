/*
 * SX1278.cpp
 *
 *  Created on: 24/07/2015
 *      Author: nikolaj
 */

#include "SX1278.h"

const unsigned char SX1278::sx1276_7_8FreqTbl[1][3] = { { 0x6C, 0x80, 0x00 }, //434MHz
		};

const unsigned char SX1278::sx1276_7_8PowerTbl[4] = { 0xFF,              //20dbm
		0xFC,                   //17dbm
		0xF9,                   //14dbm
		0xF6,                   //11dbm
		};
const unsigned char SX1278::sx1276_7_8SpreadFactorTbl[7] = { 6, 7, 8, 9, 10, 11,
		12 };

const unsigned char SX1278::sx1276_7_8LoRaBwTbl[10] = {
//7.8KHz,10.4KHz,15.6KHz,20.8KHz,31.2KHz,41.7KHz,62.5KHz,125KHz,250KHz,500KHz
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
const char* SX1278::sx1276_7_8Data = "Mark1 Lora sx1276_7_8";

SX1278::SX1278() {
	// TODO Auto-generated constructor stub

}

SX1278::~SX1278() {
	// TODO Auto-generated destructor stub
}

