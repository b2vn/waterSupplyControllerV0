/*
 * Shifty.h
 *
 *  Created on: 29/06/2015
 *      Author: nikolaj
 */

#ifndef SHIFTY_H_
#define SHIFTY_H_

#include <Arduino.h>
#include "settings.h"

class Shifty {
public:
	Shifty();
	virtual ~Shifty();
	void shift(bool state) {
	  digitalWrite(DATA_PIN, state);
	  __asm__("nop\n");
	  digitalWrite(DATA_CLOCK, HIGH);
	  __asm__("nop\n");
	  digitalWrite(DATA_CLOCK, LOW);
	}

	void latch() {
	  digitalWrite(DATA_LATCH, HIGH);
	  __asm("nop\n");
	  digitalWrite(DATA_LATCH, LOW);
	}
};

#endif /* SHIFTY_H_ */
