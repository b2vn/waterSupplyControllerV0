/*
 * settings.h
 *
 *  Created on: 29/06/2015
 *      Author: nikolaj
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

// Configuration
#define DEBUG_FLOW false
#define DEBUG_2G4 false

#define USE_BT false
#define USE_NRF24 true
#define USE_LORA false

// Bluetooth
#if USE_BT
#	define BT_NAME "GardenWaterer"
#	define BT_RX 5 // board tx
#	define BT_TX 4 // board rx
#endif

#define NUMBER_OF_CHANNELS 48

// Pins
#define FLOW_SENSOR_PIN 2 // NOTE: This pin is chosen because it has PC_INT
#define START_BUTTON_PIN 3 // NOTE: This pin is chosen because it has PC_INT
#define OUTPUT_ENABLE A0

// Shift register
#define DATA_PIN 8
#define DATA_CLOCK 6
#define DATA_LATCH 7


// Ammounts
#define TREE        20
#define BIG_BUSH    10
#define MEDIUM_BUSH  7
#define SMALL_BUSH   5
#define SMALL_TREE   5

#define L2P(d) ((int)(1000.0*d))


#endif /* SETTINGS_H_ */
