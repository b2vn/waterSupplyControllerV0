/*
 * BT Module blinks quickly: Means the bt module is not connected
 */
#include "Arduino.h"
#include "settings.h"

#include <SoftwareSerial.h>
#include <TimerOne.h>
#include <avr/sleep.h>
#include <avr/power.h>

// To be osed by the RF24 module
#if USE_NRF24
#	include <SPI.h>
#	include "nRF24L01.h"
#	include "RF24.h"
#	include "printf.h"

RF24 radio(9, 10);
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
#elif USE_LORA
#	include "SX1272.h"
#include "LoraHappyGoLucky.h"
LoraHappyGoLucky sx1272;
#endif

#include "AlongRoadMap.h"
#include "Channels.h"
#include "Display.h"

#if USE_BT
SoftwareSerial btModule(BT_TX, BT_RX);
#endif


// Flow control
#define LOW_FLOW_RATE 2
#define LOW_FLOW_TIME 10
uint32_t flow = 0;
uint32_t flowRate = 0;
uint8_t lowFlowCount = 0;

uint8_t buttonPushTime = 0;

Display display;
 AlongRoad m;
//#include "OneLiter32.h"
//OneLiter32 m;
Channels c(NUMBER_OF_CHANNELS, &m);

bool updateFlow() {
	static uint32_t lastFlow = 0;

	flowRate = flow - lastFlow;
	lastFlow = flow;

	// Verify that water is still running, else stop talking
	if (flowRate <= LOW_FLOW_RATE && c.anyOn()) {
		if (lowFlowCount <= LOW_FLOW_TIME) {
			++lowFlowCount;
		} else {
			// Low flow for too long time, stop the watering
			return true;
		}
	} else {
		lowFlowCount = 0;
	}

	display.printFlow(flowRate, lowFlowCount);

	return false;
}

void sendWaterOn(uint8_t on) {
#if USE_NRF24
	static bool running = false;
	if (running)
		return;
	running = true;

	display.printFaucet(on);

	// TODO: The waterer must also report the flowrate to the fauset. This way it will be possible
	// to output the water slow enough to make sure that the water can sink down, rather that just
	// flow away from the intended plant.
	// updateFlow();

#if DEBUG_2G4
	Serial.print("Request water: ");
	if (on)
	Serial.println("on");
	else
	Serial.println("off");
#endif

	// Send request out
	uint32_t startedWaitingAt = millis();
	radio.stopListening();

	unsigned long data = on;
	if (radio.write(&data, sizeof(unsigned long))) {
#if DEBUG_2G4
		Serial.println("  Data send");
#endif
	} else {
#if DEBUG_2G4
		Serial.println("  Failed to send");
#endif
	}

	// Wait for pong from faucet

	radio.startListening();
	bool timeout = false;
	while (!radio.available() && !timeout)
		if (millis() - startedWaitingAt > 200)
			timeout = true;

	// radio.stopListening();

	// Describe the results
	if (timeout) {
		Serial.println("  Failed, response timed out.\n\r");
		display.printPing((uint32_t) -1);
	} else {
		unsigned long pong;
		radio.read(&pong, sizeof(unsigned long));
#if DEBUG_2G4
		Serial.print("  Faucet: on (");
		Serial.print(millis() - startedWaitingAt);
		Serial.println("ms)");
#endif
		display.printPing(millis() - startedWaitingAt);
	}

	running = false;
#endif
}

void timerIrq() {
	sei();


//	if(beenDoneForLong) {
//
//	}
//	else {

		// Detect long push
		bool btn = !digitalRead(START_BUTTON_PIN);
		if (btn) {
			Serial.println("btn");
			if (buttonPushTime < 3) {
				++buttonPushTime;
			} else {
				c.stop();
			}
		}


		if (updateFlow()) {
			c.stop();
		}

		if (c.anyOn()) {
			sendWaterOn(true);
			display.printTotal(flow / L2P(1));
			display.printCurrent(c.getCurrent()+1, c.getProcentage());
		} else {
//			sendWaterOn(false);
			display.printCurrent(-1, -1);
//			display.off();
		}
//	}
}

void incrementFlow() {
	++flow;
#if DEBUG_FLOW
	Serial.print("Total flow: ");
	Serial.println(flow);
#endif

	bool res = c.increment();

	if ((flow % 100) == 0) {

		if (res) {
			Serial.println("All done");
//    sendWaterOn(false);
		}
	}
}

void startButton() {
	flow = 0;

	if (!digitalRead(START_BUTTON_PIN)) {
		// Button pushed
		buttonPushTime = 0;
	} else {
		if (!c.anyOn() && buttonPushTime < 3) {
			Serial.println("Start button pushed");

			lowFlowCount = 0;
			c.resetAll();
			c.openNext();

			// Initialize display
			sei();
			display.init();
		}
	}
}

void enterSleep(void) {
	set_sleep_mode(SLEEP_MODE_IDLE);

	sleep_enable()
	;

	/* Disable all of the unused peripherals. This will reduce power
	 * consumption further and, more importantly, some of these
	 * peripherals may generate interrupts that will wake our Arduino from
	 * sleep!
	 */
	power_adc_disable();
//	power_spi_disable();
	power_timer0_disable();
	power_timer2_disable();
//	power_twi_disable();

	/* Now enter sleep mode. */
	sleep_mode()
	;

	/* The program will continue from here after the timer timeout*/
	sleep_disable()
	; /* First thing to do is disable sleep. */

	/* Re-enable the peripherals. */
	power_all_enable();
}

void setup() {
	// Very important - first turn off the output for the valves
	pinMode(OUTPUT_ENABLE, OUTPUT);
	digitalWrite(OUTPUT_ENABLE, LOW);

	// Display
	display.begin();
	delay(50);
	display.booting();

	// Serial monitor
	Serial.begin(57600);
	while (!Serial)
		;

	// Setup RF module
	Serial.println("Start radio link");
#if USE_NRF24
	printf_begin();
	radio.begin();
	radio.setRetries(15, 15);
	radio.openWritingPipe(pipes[0]);
	radio.openReadingPipe(1, pipes[1]);
	radio.startListening();
	radio.printDetails();
	delay(100);
#elif USE_LORA
//	  sx1272.ON();
//	  sx1272.setMode(4);
//	  sx1272.setHeaderON();
//	  sx1272.setChannel(CH_10_868);
//	  sx1272.setCRC_ON();
//	  // Select output power (Max, High or Low)
//	  sx1272.setPower('H');
//	  sx1272.setNodeAddress(3);
#endif

#if USE_BT
	// Bluetooth
	Serial.println("Start BT communication");

	btModule.begin(9600);
	delay(1000);

	btModule.print("AT");
	delay(1000);

	/*
	 // Should respond with its version
	 mySerial.print("AT+VERSION");
	 delay(1000);

	 // Set pin to 0000
	 mySerial.print("AT+PIN0000");
	 delay(1000);
	 */

	// Set the name
	btModule.print("AT+NAME");
	btModule.print(BT_NAME);
	delay(1000);

	// Set baudrate to 57600
	//  mySerial.print("AT+BAUD7");
	//  delay(1000);
#endif

	// ISR
	pinMode(FLOW_SENSOR_PIN, INPUT);
	digitalWrite(FLOW_SENSOR_PIN, HIGH);

	pinMode(START_BUTTON_PIN, INPUT);
	digitalWrite(START_BUTTON_PIN, HIGH);

	// Clear interruptflags before attaching the interrupts
	EIFR |= (1 << INTF0); //Added
	EIFR |= (1 << INTF1); //Added
	attachInterrupt(INT0, incrementFlow, CHANGE);
	attachInterrupt(INT1, startButton, CHANGE);

	delay(2000);

	// Water on request timer
	Timer1.initialize(1000000UL);
	Timer1.attachInterrupt(timerIrq);

	display.booted();
	Serial.println("Init done !");

	display.init();

}

#if USE_BT
String btCommand = "";
#endif
uint32_t lastPing = 0;

void loop() {
#if USE_BT
	while (Serial.available())
	btModule.write(Serial.read());
	while (btModule.available())
	Serial.write(btModule.read());
#endif


//	 enterSleep();
	/*
	 // Parse BT commands
	 while(btModule.available()) {
	 byte c = btModule.read();
	 Serial.write(c);

	 if(c != 13) { // 13: line brake
	 btCommand += c;
	 }
	 else {
	 // New command received
	 // Make a local copy of the command, and make ready for receiving a new one... TODO: This is only efficient in the UART is interrupt driven
	 String tp(btCommand);
	 btCommand = "";

	 // Parse command
	 uint8_t id;
	 uint16_t amount;
	 sscanf(tp.c_str(), "%d:%d", &id, &amount);
	 w.setAmount(id, amount);
	 }
	 }
	 */
}
